const { Router } = require("express");
const sdk = require("facebook-nodejs-business-sdk");

const accessToken = process.env.FACEBOOK_ACCESS_TOKEN;
const accountId = process.env.FACEBOOK_AD_ACCOUNT_ID;

sdk.FacebookAdsApi.init(accessToken);
const { AdAccount, Campaign } = sdk;

const account = new AdAccount(`act_${accountId}`);

const api = db => {
  const router = Router();

  const collection = db.collection("campaigns");

  router.put("/create", async (req, res) => {
    const { name, type } = req.query;

    try {
      const campaign = await account.createCampaign([], {
        [Campaign.Fields.name]: name,
        [Campaign.Fields.objective]: type
      });

      const result = await collection.insertOne({
        campaignId: campaign.id,
        name,
        type
      });
      res.send(result);
    } catch (error) {
      res.send(error);
    }
  });

  router.delete("/delete", async (req, res) => {
    try {
      // Get all campaigns from db and make a request to fb for each one to delete it
      // Then delete them all from the db.
      const campaigns = await collection.find().toArray();
      const deletions = await Promise.all(
        campaigns.map(({ campaignId }) => {
          return new Campaign(campaignId)
            .delete()
            .then(response => response)
            .catch(() => ({ success: false }));
        })
      );

      // If a delete-reqest returns an error, we don't remove it from the db either
      // so everything stays in sync
      const deletedCampaignIds = campaigns
        .filter((campaign, i) => deletions[i].success)
        .map(campaign => campaign.campaignId);

      const result = await collection.deleteMany({
        campaignId: {
          $in: deletedCampaignIds
        }
      });
      res.send(result);
    } catch (error) {
      res.send(error);
    }
  });

  return router;
};

module.exports = api;
