const { MongoClient } = require("mongodb");

(async () => {
  const client = await MongoClient.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true
  });
  const db = client.db(process.env.MONGODB_NAME);

  await db.createCollection("campaigns", {
    validator: {
      $jsonSchema: {
        bsonType: "object",
        required: ["campaignId", "name", "type"],
        properties: {
          name: { bsonType: "string" },
          campaignId: { bsonType: "string" },
          type: { bsonType: "string" }
        }
      }
    },
    validationAction: "error",
    validationLevel: "strict"
  });

  client.close();
})();
