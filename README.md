## Prerequisites

To run this server, you need to have the following programs installed

- MongoDB >=3.6 or later
- NodeJS >=8.x
- Yarn
- Postman

## Setup

### Environmen variables

Start up a mongodb server locally on port 27017 and create a database named _adway_ or any name you prefer. Then create a .env file in this repository and fill it out like this so the api can connect to the db you just created:

```
MONGODB_URL=mongodb://localhost:27017
MONGODB_NAME=adway
```

In order to connect to my facebook developer account we'll have to add those to the .env file too. They should really be kept out of source control, but who cares about security.

```
FACEBOOK_ACCESS_TOKEN=EAAhG643ZCKBoBAFHhpbpGEh5yC9KCREP6wyHZCFeZCD6S9BlqP8Hq1ZCRVGfyF27JK6WyLJaR60dcywDqiMcvJYVQDCiBkOvIzk9kxMhaULoAl5HDDKjvJv3TQtfuZAmweAn1uCOllGrXt0C9UR88ZCAzeEYkOvN0ZARaKVrTUaGjovkZC7qfSFf
FACEBOOK_AD_ACCOUNT_ID=342057189761758
```

### Build

```bash
yarn # Install dependencies
yarn create-db # Create the campaigns-collection.
yarn dev # Run the server with nodemon.
```

## Use

Open Postman and make a POST-request to `http://localhost:3000/create?name=<name of campaign>&type=<objective>` to create a new facebook campaign.

To delete all campaigns from both facebook and the db, make a DELETE-request to `http://localhost:3000/delete`
