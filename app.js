const express = require("express");
const { MongoClient } = require("mongodb");
const logger = require("morgan");
const api = require("./api");

const app = express();
(async () => {
  const client = await MongoClient.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true
  });
  const db = client.db(process.env.MONGODB_NAME);

  app.use(logger("tiny"));
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use("/", api(db));

  const port = process.env.PORT || 3000;
  app.listen(port, () => console.log(`Server listening on port ${port}`));
})();
